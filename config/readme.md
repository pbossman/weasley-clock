# to mysql server:
mysql-ctl start
mysql-ctl cli

# Start weasley clock node-red instance
node-red &

# start the weasley clock node server
node clocksrv.js &      
