/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var app = express();

var env = process.env.NODE_ENV || 'development';
var config = require('./config/config')[env];
var con = mysql.createConnection({
  host: config.database.host,
  user: config.database.user,
  password: config.database.password,
  database: config.database.dbname
}); 

// all environments
app.use(bodyParser.json({type: 'application/json'}));
app.set('port', process.env.PORT || 3000);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'client')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// retrieve users positions for a clock
app.get('/getClockData/clockID/:clockID', function(request, response){
  var selClockData = "SELECT User, Location FROM Test WHERE Clock = ? ORDER BY User ASC";
  con.connect(function(err) {
    console.log("Connected!");
    con.query(selClockData, [ request.params.clockID ], function (err, result) {
      if (err) throw err;
      response.send(result);
    });
  });
});

app.post('/location', express.json(), function(req, res){
  var insLandmark = '';
  var x = req.body;
  con.connect(function(err) {
    console.log("insert values ", x );
//    if (err) throw err;
    console.log("Connected!");
    insLandmark = "INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius) Values (?, ?, ?, ?, ?, ?)";
    con.query(insLandmark, [ 1, x.loccatID, x.locname, x.loclat, x.loclon, x.locradius ], function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});

// retrieve users positions for a clock
app.get('/clockCat/clockID/:clockID', function(request, response){
  var selStmt = "select C.Category, CP.Position from Categories C, ClockPosition CP WHERE C.ID = CP.CategoryID AND ClockID = ?";
  con.connect(function(err) {
    console.log("Connected!");
    con.query(selStmt, [ request.params.clockID ], function (err, result) {
      if (err) throw err;
      response.send(result);
    });
  });
});

// retrieve categories 
app.get('/categories/clockID/:clockID', function(request, response){
  var selStmt = "select C.Category, C.ID AS CategoryID, COALESCE(CP.Position,-1) AS ClockPosition ";
  selStmt = selStmt + "from Categories C LEFT OUTER JOIN ClockPosition CP ";
  selStmt = selStmt + "on C.ID = CP.CategoryID AND ClockID = ?";
  con.connect(function(err) {
    console.log(err)
    con.query(selStmt, [ request.params.clockID ], function (err, result) {
      if (err) throw err;
      response.send(result);
    });
  });
});

// retrieve fact
//Fact (name, shortFact, moreFact, longFact, factType, lat, lon, RelevanceRadius)
app.get('/fact', function(request, response){
  var selStmt = "select name, shortFact, moreFact, longFact, factType, lat, lon, RelevanceRadius from fact";
  con.connect(function(err) {
    console.log("sql submitted " + selStmt)
    con.query(selStmt, function (err, result) {
      if (err) throw err;
      console.log(result)
      response.send(result);
    });
  });
});
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
