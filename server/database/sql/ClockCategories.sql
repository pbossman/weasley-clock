select C.Category, CP.Position, CP.CategoryID
from Categories C, ClockPosition CP 
WHERE C.ID = CP.CategoryID 
AND ClockID = 1
ORDER BY CP.Position;