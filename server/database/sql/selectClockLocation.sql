SELECT C.Category, L.Name, L.Lat, L.Lon, L.Radius
FROM Location L
    , Categories C
    , ClockPosition CP
WHERE CP.ClockID = 1
AND   C.ID = CP.CategoryID
AND   L.ClockID = CP.ClockID
AND   L.CategoryID = C.ID;