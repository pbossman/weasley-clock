CREATE TABLE Categories
  (ID bigint
  , Category varchar(128)
  );

INSERT INTO Categories (ID, Category) VALUES (1, 'HOME');
INSERT INTO Categories (ID, Category) VALUES (2, 'WORK');
INSERT INTO Categories (ID, Category) VALUES (3, 'SCHOOL');
INSERT INTO Categories (ID, Category) VALUES (4, 'COFFEE');
INSERT INTO Categories (ID, Category) VALUES (5, 'LIBRARY');
INSERT INTO Categories (ID, Category) VALUES (6, 'GROCERY');
INSERT INTO Categories (ID, Category) VALUES (7, 'YOGA');
INSERT INTO Categories (ID, Category) VALUES (8, 'BEACH');
INSERT INTO Categories (ID, Category) VALUES (9, 'PET'); 
INSERT INTO Categories (ID, Category) VALUES (10, 'G&G'); 
INSERT INTO Categories (ID, Category) VALUES (11, 'TRAVELING'); 
INSERT INTO Categories (ID, Category) VALUES (12, 'UNKNOWN'); 
INSERT INTO Categories (ID, Category) VALUES (13, 'PARK'); 
INSERT INTO Categories (ID, Category) VALUES (14, 'AMUSEMENT'); 