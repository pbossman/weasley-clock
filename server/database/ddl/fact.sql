CREATE TABLE fact
  (ID INTEGER PRIMARY KEY AUTO_INCREMENT
  ,name VARCHAR (128)
  ,shortFact TEXT DEFAULT NULL
  ,moreFact TEXT DEFAULT NULL
  ,longFact TEXT DEFAULT NULL
  ,factType TEXT DEFAULT NULL
  ,lat DECIMAL (18,15)
  ,lon DECIMAL (18,15)
  ,RelevanceRadius INTEGER DEFAULT NULL
  );

/* Default "insert" statement:
INSERT INTO Fact (name, shortFact, moreFact, longFact, factType, lat, lon, RelevanceRadius)
VALUES ("__", 
        "_______", 
        "_________",
        "_______________",
        "__"
        ..., ..., ...);


*/
INSERT INTO fact (name, shortFact, moreFact, longFact, factType, lat, lon, RelevanceRadius)
VALUES ("bossmanHouse", 
        "The Bossmans live in this very neighborhood", 
        "The Bossmans are who had the idea for this app. They live at the end of this neighborhood.",
        "The Bossmans are a fabulous family. They had the idea for this app. They live in this neighborhood in the house in the cul-de-sac.",
        "Neighborhood",
        27.2784386,-82.5489937, 500);

INSERT INTO fact (name, shortFact, moreFact, longFact, factType, lat, lon, RelevanceRadius)
VALUES ("Lelu Coffee", 
        "Lelu Coffee is a coffee shop in the village", 
        "_________",
        "_______________",
        "__"
        27.2745, -82.5641, 1000);


/*
Our House lat: 27.2784386
          lon: -82.5489937
*/