CREATE TABLE LocationLog
  (User varchar(4)
  ,Lat decimal (18,15)
  ,Lon decimal (18,15)
  ,Batt integer
  ,LocationTS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  );