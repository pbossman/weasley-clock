CREATE TABLE Location
  (ClockID bigint
  ,CategoryID bigint
  ,Name varchar(128)
  ,Lat decimal (18,15)
  ,Lon decimal (18,15)
  ,Radius integer
  );

INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 1, '5125 Siesta Woods Drive',  27.2784386,-82.5489937, 200);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 13, 'Glebe Park',  27.273998,-82.5499071, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 3, 'Phillippi Shores',  27.2854135,-82.5287698, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 3, 'Pine View',  27.1860944,-82.4815321, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 4, 'Lelu Coffee',  27.2745378,-82.5662514, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 4, 'Starbucks near Phillippi',  27.2765135,-82.5566411, 150);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 6, 'Publix North Bridge',  27.2765116,-82.5566411, 250);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 6, 'Publix South Bridge',  27.2786347,-82.5541952, 250);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 6, 'Publix Palmer Ranch',  27.228464, -82.491483, 250);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 6, 'Trader Joes',  27.277936,-82.550139, 250);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 7, 'Prana Yoga',  27.2994008,-82.5361178, 150);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 7, 'Rosemary Yoga', 27.2987161,-82.5726973, 150);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 2', 27.2736904,-82.568963, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 3', 27.273523, -82.567676, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 5', 27.271689, -82.565782, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 5', 27.271227, -82.565052, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 7', 27.270482, -82.563996, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 7', 27.269972, -82.563339, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 8', 27.268720, -82.561803, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Beach Access 8', 27.268443, -82.561303, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Main Beach', 27.267159, -82.559282, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Main Beach', 27.266540, -82.555770, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Main Beach', 27.265464, -82.554388, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Main Beach', 27.265038, -82.553121, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Main Beach', 27.264271, -82.551619, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 8, 'Siesta Main Beach', 27.263576, -82.550443, 400);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 9, 'Cat Depot', 27.3518649,-82.5215092, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 9, 'Humane Society', 27.3509024,-82.525571, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 9, 'Vet', 27.2278726,-82.4936235, 200);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 9, 'Petco Northbridge', 27.300053, -82.531269, 200);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 9, 'Petco South', 27.2467647,-82.5160247, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 10, 'G&G', 27.2456646,-82.5070952, 300);
INSERT INTO Location (ClockID, CategoryID, Name, Lat, Lon, Radius)
VALUES (1, 11, 'SRQ', 27.3950478,-82.5606658, 2000);