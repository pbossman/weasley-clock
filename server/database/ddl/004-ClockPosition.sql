CREATE TABLE ClockPosition
  (ClockID bigint
  ,Position smallint
  ,CategoryID bigint
  );

INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 1, 1);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 2, 2);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 3, 3);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 4, 4);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 5, 5);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 6, 6);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 7, 7);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 8, 8);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 9, 9);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 10, 10);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 11, 11);
INSERT INTO ClockPosition (ClockID, Position, CategoryID) VALUES (1, 12, 12);