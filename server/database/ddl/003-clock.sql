CREATE TABLE Test
  ( Clock bigint
  , User integer
  , Location integer
  );

INSERT INTO Test (Clock, User, Location) VALUES (1 , 1 , 1);
INSERT INTO Test (Clock, User, Location) VALUES (1 , 2 , 3);
INSERT INTO Test (Clock, User, Location) VALUES (1 , 3 , 6);
INSERT INTO Test (Clock, User, Location) VALUES (1 , 4 , 8);
INSERT INTO Test (Clock, User, Location) VALUES (1 , 5 , 11);
