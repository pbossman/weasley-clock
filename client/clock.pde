/* define global variables */

var radius = 350;

/* define dot r g b are colors. */
var dot = function(r, g, b) {
  this.r = r;
  this.g = g;
  this.b = b;
};

/* define dot draw function. */
/* input: x and y are position of dot. */
dot.prototype.draw = function(x, y) {
  this.x = x;
  this.y = y;
  noStroke();
  fill(this.r, this.g, this.b);

  // radius of dot is variable of clock size.
  // could make this a variable to allow larger/smaller dots.
  ellipse(this.x, this.y, radius/50, radius/50);
};

/* define people in clock.   */
/* Should change users into an array of variable length based on number of users for particular clock. */
var user1 = new dot(0, 206, 209);
var user2 = new dot(255, 255, 0);
var user3 = new dot(0, 128, 0);
var user4 = new dot(255, 0, 255);
var user5 = new dot(255, 255, 255);

/* Size is currently fixed.  */
void setup() {
  size(400, 400);
}

void draw(){
  textAlign(CENTER, CENTER);
  fill(168, 96, 168); //color picker
  
  /* Not sure what this code is doing yet.  */
  stroke(175, 140, 5);
  fill(255, 255, 255);
  strokeWeight(radius/17.5);
  ellipse(200, 200, radius, radius);
  fill(0);
  textFont(createFont("serif"), radius/30);

  /* Middle of clock - positions noon and 6 o'clock */
  textAlign(CENTER, CENTER);
  text(where[0], width/2, (height/2)-((radius/35)*14));
  text(where[6], width/2, (height/2)+((radius/35)*14));

  /* Right side of clock - positions 1 through 5 */
  textAlign(RIGHT, CENTER);
  strokeWeight(5);
  text(where[1], (width/2)+((radius/35)*13), (height/2)-((radius/35)*9));
  text(where[2], (width/2)+((radius/7)*3), (height/2)-((radius/35)*4.5));
  text(where[3], (width/2)+(radius/2.2), height/2);
  text(where[4], (width/2)+((radius/7)*3), (height/2)+((radius/35)*4.5));
  text(where[5], (width/2)+((radius/35)*13), (height/2)+((radius/35)*9));

  /* Left side of clock - positions 7 through 11 */
  textAlign(LEFT, CENTER);
  text(where[7], (width/2)-((radius/35)*13), (height/2)+((radius/35)*9));
  text(where[8], (width/2)-((radius/7)*3), (height/2)+((radius/35)*4.5));
  text(where[9], (width/2)-(radius/2.2), height/2);
  text(where[10], (width/2)-((radius/7)*3), (height/2)-((radius/35)*4.5));
  text(where[11], (width/2)-((radius/35)*13), (height/2)-((radius/35)*9));

  /* Not sure what this code is doing.  */
  var userX = [width/2, width/2+radius/35*11.2, width/2+(radius/10)*3.75, width/2+radius/35*13.5, 0, width/2+radius/10*3, 0, width/2-radius/35*10.5, width/2-radius/35*13, width/2-radius/35*14, width/2-radius/10*3.8, width/2-radius/35*11.5];
  var userY = [height/2-radius/35*13, height/2-radius/35*8, radius/7*3+radius/25, height/2+radius/35, height/2+radius/35*5.5, height/2+radius/35*10, height/2+radius/35*15];

  /* Draw the user dots. */
  /* Variable user enhancement:  Convert to an array where number of users is determined from read of database. */
  /* This will also require the position of dots be converted to algorithm, or define position of pre-determined number of dots scenarios  */
  /* Eg. have different draw functions based on number of clock dots, and user particular function based on number of users */
  /* The most elegant is if the algorithm self-adjusted based on number of users */
  user1.draw(userX[userArr[0].x]-radius/70*3, userY[userArr[0].y]);
  user2.draw(userX[userArr[1].x]-radius/70*1.5, userY[userArr[1].y]);
  user3.draw(userX[userArr[2].x], userY[userArr[2].y]);
  user4.draw(userX[userArr[3].x]+radius/70*1.5, userY[userArr[3].y]);
  user5.draw(userX[userArr[4].x]+radius/70*3, userY[userArr[4].y]);
}